var HtmlWebpackPlugin = require('html-webpack-plugin')
var path = require('path')
var webpack = require('webpack')
module.exports = {
    entry: './index.js', // entry point of our application
    output: {
        filename: '/app/bundle.js', 
        // path of the bundled file
        path: path.resolve(__dirname, 'dist')
    },
    module: { // rules for bundling our files
        rules: [
            {
                test: /.js$/, // what to look for
                exclude: /node_modules/,
                use: [
                    'babel-loader'
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
           title: 'Alfa Bravo',
           template: 'index.html',
        }),
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.DefinePlugin([
            new webpack.EnvironmentPlugin({
              NODE_ENV: 'production'
            })
        ])   
    ],
    "scripts": {
        "dev": "webpack-dev-server --hot",
        "build": "webpack --mode production"
    }
}